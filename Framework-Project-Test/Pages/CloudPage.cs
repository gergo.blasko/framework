using OpenQA.Selenium;

public class CloudPage
{
    public string NumberOfInstances { get; }
    public string NumberOfNodes { get; }
    public string FirstResult { get; }
    public string SearchInputField { get; }
    public string SearchButton { get; }
    public string Url { get; }
    public string Frame1 { get; }
    public string Frame2 { get; }
    public string SubmissionButton { get; }

    public string SubmissionButton2 { get; }
    public string EmailEstimate { get; }
    public string EmailField { get; }
    public string SendEmailButton { get; }
    public CloudPage()
    {
        NumberOfInstances = @"//*[@id=""input_99""]";
        FirstResult = @"a[class=""gs-title""]";
        SearchInputField = @"input[aria-label=""Search""]";
        SearchButton = @"div[jslog=""169102;track:impression,click;fYPJpb:W1tudWxsLCIvIl1d;""]";
        Url = @"https://cloud.google.com/";
        Frame1 = @"//*[@id=""cloud-site""]/devsite-iframe/iframe";
        Frame2 = @"myFrame";
        SubmissionButton = @"button[ng-click=""listingCtrl.addComputeServer(ComputeEngineForm);""]";
        EmailEstimate = @"//*[@id=""Email Estimate""]";
        NumberOfNodes = @"input[ng-model=""listingCtrl.soleTenant.nodesCount""]";
        SubmissionButton2 = @"/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[2]/form/div[13]/button";
        EmailField = @"input[type=""email""]";
        SendEmailButton = @"button[ng-disabled=""emailForm.$invalid""]";
    }
    public IWebElement ReturnSendEmailButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SendEmailButton));
    }
    public IWebElement ReturnEmailField(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(EmailField));
    }
    public IWebElement ProvideNumberOfNodes(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(NumberOfNodes));
    }
    public IWebElement ProvideNumberOfInstances(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(NumberOfInstances));
    }
    public IWebElement SearchForFirstResult(IWebDriver driver)
    {
        return driver.FindElements(By.CssSelector(FirstResult))[0];
    }
    public IWebElement SearchForSearchInputField(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SearchInputField));
    }
    public IWebElement SearchForSearchButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SearchButton));
    }
    public IWebElement SearchForFrame1(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(Frame1));
    }
    public IWebElement SearchForFrame2(IWebDriver driver)
    {
        return driver.FindElement(By.Id(Frame2));
    }
    public IWebElement SearchForSubmissionButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SubmissionButton));
    }
    public IWebElement SearchForSubmissionButton2(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector("button[ng-transclude]"));

    }
    public IWebElement SearchForEmailEstimateButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(EmailEstimate));
    }
}