using OpenQA.Selenium;

public class YopMailPage
{
    public string Url { get; }
    public YopMailPage()
    {
        Url = @"https://yopmail.com/";
    }
    public IWebElement AcceptButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(@"button[id=""accept""]"));
    }
    public IWebElement LoginButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(@"//*[@id=""login""]"));
    }

}