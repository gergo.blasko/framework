﻿using NUnit.Framework;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

using System.Net.Http;
using System.Threading.Tasks;
using System.Text;


[TestFixture]
class TestClass
{
    CloudPage cloudPage;
    YopMailPage yopMailPage;
    IWebDriver webDriver;

    [SetUp]
    public void Init()
    {
        webDriver = new ChromeDriver();
        yopMailPage = new YopMailPage();
        cloudPage = new CloudPage();
        webDriver.Manage().Window.Maximize();
    }

    [Test]
    public void Test_If_The_Task_Can_Be_Executed()
    {
        // Arrange
        Actions a = new Actions(webDriver);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        // Act
        webDriver.Navigate().GoToUrl(cloudPage.Url);

        cloudPage.SearchForSearchButton(webDriver).Click();

        cloudPage.SearchForSearchInputField(webDriver).SendKeys("Google Cloud Platform Pricing Calculator");

        cloudPage.SearchForSearchInputField(webDriver).SendKeys(Keys.Enter);

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        cloudPage.SearchForFirstResult(webDriver).Click();

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        webDriver.SwitchTo().Frame(cloudPage.SearchForFrame1(webDriver));

        webDriver.SwitchTo().Frame(cloudPage.SearchForFrame2(webDriver));

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        cloudPage.ProvideNumberOfInstances(webDriver).SendKeys("4");

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        cloudPage.ProvideNumberOfNodes(webDriver).SendKeys("4");

        Thread.Sleep(10000);

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        cloudPage.SearchForSubmissionButton(webDriver).Click();

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

        cloudPage.SearchForEmailEstimateButton(webDriver).Click();

        ((IJavaScriptExecutor)webDriver).ExecuteScript("window.open();");

        webDriver.SwitchTo().Window(webDriver.WindowHandles.Last());

        webDriver.Navigate().GoToUrl(yopMailPage.Url);

        try
        {
            yopMailPage.AcceptButton(webDriver).Click();
        }
        catch
        {

        }

        var address = "tinyurl93";

        yopMailPage.LoginButton(webDriver).SendKeys(address);

        yopMailPage.LoginButton(webDriver).SendKeys(Keys.Enter);

        webDriver.SwitchTo().Window(webDriver.WindowHandles.First());

        System.Console.WriteLine(webDriver.Url);

        System.Console.WriteLine(webDriver.Url[48..]);

        // Define the request URI and content
        string requestUri = "https://cloudpricingcalculator.appspot.com/quote/mail";
        string jsonContent = $@"{{
    ""email"": ""tinyurl93@yopmail.com"",
    ""project_url"": """",
    ""project"": """",
    ""total_price"": 195.6733464,
    ""id"": ""{webDriver.Url[48..]}"",
    ""requested_currency"": ""USD""
}}";
        System.Console.WriteLine(jsonContent);
        int contentLength = Encoding.UTF8.GetByteCount(jsonContent);

        // Create an HttpClient instance
        using (HttpClient client = new HttpClient())
        {
            // // Set the request headers
            client.DefaultRequestHeaders.Add("Host", "cloudpricingcalculator.appspot.com");
            // client.DefaultRequestHeaders.Add("Sec-Ch-Ua", "\"Not=A?Brand\";v=\"99\", \"Chromium\";v=\"118\"");
            client.DefaultRequestHeaders.Add("Accept", "*/*");
            //client.DefaultRequestHeaders.Add("Content-Length", contentLength.ToString());
            //client.DefaultRequestHeaders.Add("Content-Type", "text/plain");

            client.DefaultRequestHeaders.Add("Connection", "keep-alive");
            // client.DefaultRequestHeaders.Add("Sec-Ch-Ua-Mobile", "?0");
            // client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.90 Safari/537.36");
            // client.DefaultRequestHeaders.Add("Sec-Ch-Ua-Platform", "\"macOS\"");
            // client.DefaultRequestHeaders.Add("Origin", "https://cloudpricingcalculator.appspot.com");
            // client.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");
            // client.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "cors");
            // client.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "empty");
            // client.DefaultRequestHeaders.Add("Referer", "https://cloudpricingcalculator.appspot.com/");
            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            //client.DefaultRequestHeaders.Add("Accept-Language", "en-GB,en-US;q=0.9,en;q=0.8");

            // Create a StringContent object with the JSON data
            StringContent content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            content.Headers.Add("Content-Length", contentLength.ToString());

            System.Console.WriteLine(contentLength.ToString());

            // Send the POST request
            client.PostAsync(requestUri, content);

            // Send the POST request
            // HttpResponseMessage response = await client.PostAsync(requestUri, content);

            // // Check the response status
            // if (response.IsSuccessStatusCode)
            // {
            //     // Request was successful
            //     string responseContent = await response.Content.ReadAsStringAsync();
            //     Console.WriteLine("Request was successful. Response:");
            //     Console.WriteLine(responseContent);
            // }
            // else
            // {
            //     // Request failed
            //     Console.WriteLine("Request failed with status code: " + response.StatusCode);
            // }


            // // Check the response status code
            // if (response.IsSuccessStatusCode)
            // {
            //     // Read the response content as a string
            //     string responseContent = await response.Content.ReadAsStringAsync();
            //     Console.WriteLine("Response Content:");
            //     Console.WriteLine(responseContent);
            // }
            // else
            // {
            //     Console.WriteLine($"Request failed with status code: {response.StatusCode}");
            // }
        }

        // webDriver.SwitchTo().DefaultContent();

        // webDriver.SwitchTo().Frame(webDriver.FindElement(By.Id(@"myFrame")));

        // // cloudPage.ReturnEmailField(webDriver).SendKeys(address + "@yopmail.com");

        // IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
        // js.ExecuteScript($"document.getElementsByName('description')[2].value = '{address}@yopmail.com';");

        // cloudPage.ReturnSendEmailButton(webDriver).Click();

    }
}